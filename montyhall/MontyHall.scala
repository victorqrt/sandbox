#!/usr/bin/env -S scala-cli shebang --scala-version 3.3.0-RC3

import scala.util.Random


enum Reward:
  case Car, Goat

import Reward._


case class Door(opened: Boolean, r: Reward)


case class MontyHall(doors: Array[Door], choice: Int):

  def openGoat =
    val (picked, idx) =
      doors.zipWithIndex
           .find { case d -> i => d.r != Car && i != choice }
           .get

    this.copy(doors = doors.updated(idx, picked.copy(opened = true)))


  def reward(swap: Boolean) =
    doors.find(d => !d.opened && (swap ^ d == doors(choice)))
         .get.r


  def rewards = reward(false) -> reward(true)


object MontyHall:

  val doors =
    Array(Door(false, Car), Door(false, Goat), Door(false, Goat))


  def simulate(times: Long) =
    val runs =
      for _ <- 0L to times
      yield
        MontyHall(doors, Random.nextInt(doors.size)).openGoat.rewards

    val keep = runs.map(_(0)).count(_ == Car).toDouble / times
    s"[times: $times, keep_winrate: $keep swap_winrate: ${1 - keep}]"


  def main(args: Array[String]) =
    val times =
      if args.size == 0 then 100000
      else args(0).toLong

    // checkmate atheists
    println(MontyHall.simulate(times))
