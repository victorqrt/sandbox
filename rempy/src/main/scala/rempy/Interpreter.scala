package rempy

import cats.*
import cats.effect.*
import jep.*
import org.typelevel.log4cats.*

import Error.*


final class Interpreter[F[_] : LoggerFactory : Sync : ErrorChannel]:

  private given logger: Logger[F] = LoggerFactory.getLogger[F]

  private lazy val baos = new java.io.ByteArrayOutputStream

  private lazy val python =
    SharedInterpreter.setConfig(
      new JepConfig()
        .redirectStdErr(baos)
        .redirectStdout(baos))

    new SharedInterpreter


  def execute(code: String): F[String] =
    Sync[F]
      .blocking:
        baos.reset
        python.exec(s"import jep\n$code")
        baos.toString
      .orRaise:
        case t => InterpreterError.ExecutionError(t.toString)


object Interpreter:

  def resource[F[_] : LoggerFactory : Sync : ErrorChannel]: Resource[F, Interpreter[F]] =
    Resource.pure(new Interpreter[F])
