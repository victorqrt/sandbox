package rempy

import cats.implicits.*
import com.monovore.decline.*


case class Config(bogus: Boolean)


object Cli:

  private lazy val bogus =
    Opts
      .flag("bogus", help = "Bogus flag")
      .orFalse


  lazy val config =
    ( bogus
    ) map Config.apply
