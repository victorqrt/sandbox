package rempy

import cats.*
import cats.data.*
import cats.effect.*
import cats.implicits.*
import com.monovore.decline.effect.*
import org.typelevel.log4cats.*
import org.typelevel.log4cats.slf4j.*

import Error.*


object Main extends CommandIOApp
  ( name    = "rempy"
  , header  = "a thing"
  , version = "0.1"):


  type Stack = [X] =>> EitherT[IO, Error, X]

  private given LoggerFactory[Stack] = Slf4jFactory.create[Stack]


  val program =
    for
      py  <- Interpreter.resource[Stack]
      web <- Web.resource[Stack](py)
    yield
      web


  def run(conf: Config) =
    program
      .use(_.server.useForever)
      .value
      .as(ExitCode.Success)


  def main =
    Cli
      .config
      .map(run)


