package rempy

import cats.*
import cats.mtl.implicits.*
import cats.implicits.*
import cats.effect.*
import com.comcast.ip4s.*
import fs2.io.net.Network
import io.circe.Encoder
import io.circe.syntax.*
import org.http4s.*
import org.http4s.circe.CirceEntityCodec.*
import org.http4s.circe.*
import org.http4s.dsl.*
import org.http4s.dsl.io.*
import org.http4s.ember.server.*
import org.http4s.server.*
import org.typelevel.log4cats.*
import org.typelevel.log4cats.syntax.*

import Error.*
import Error.given
import Web.*


final class Web
  [F[_] : Async : ErrorChannel : LoggerFactory : Network]
  (val pyInt: Interpreter[F]):

  private given logger: Logger[F] = LoggerFactory.getLogger[F]


  private val routes =
    HttpRoutes.of[F]:

      case GET -> Root =>
        for
          _ <- info"It works"
        yield
          Response[F](Status.Ok).withEntity("Yo")

      case GET -> Root / "hello" =>
        for
          _   <- info"It works"
          out <- pyInt.execute("print('hello world from python')")
        yield
          Response[F](Status.Ok).withEntity(out)


  lazy val server =
    EmberServerBuilder
      .default[F]
      .withHost(ipv4"127.0.0.1")
      .withPort(port"8080")
      .withHttpApp(Router("/" -> routes).orNotFound)
      .withLogger(logger)
      .build


object Web:

  def resource
    [F[_] : Async : ErrorChannel : LoggerFactory : Network]
    (pyInt: Interpreter[F]): Resource[F, Web[F]] =
      Resource.pure(new Web[F](pyInt))

/*
  extension [A, F[_] : ErrorChannel as errChan : MonadThrow] (fa: F[A])

    //def response(using Encoder[A]): F[Response[F]] =
    //  fa.map(Response[F](Status.Ok).withEntity)
    //   .handle[Error](Response[F](Status.InternalServerError).withEntity)

    def response(using Encoder[A]): F[Response[F]] =
      errChan.handleWith(fa.map(Response[F](Status.Ok).withEntity))
        (Response[F](Status.InternalServerError).withEntity)

    def response(show: A => String): F[Response[F]] =
      fa.map(a => Map("msg" -> show(a)).asJson).response
*/
