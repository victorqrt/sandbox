package rempy

import cats.*
import cats.implicits.*
import io.circe.Encoder
import io.circe.generic.semiauto.*
import io.circe.syntax.*
import org.typelevel.log4cats.*
import org.typelevel.log4cats.syntax.*
import tofu.*


object Error:

  type Error = ServiceError | InterpreterError

  type ErrorChannel = [F[_]] =>> Errors[F, Error]


  enum ServiceError:
    case Generic(msg: String)


  enum InterpreterError:
    case SyntaxError(msg: String)
    case ExecutionError(msg: String)


  extension [A, F[_] : ErrorChannel as errChan : Logger : MonadThrow] (fa: F[A])

    def orRaise(err: Error) =
      fa.handleErrorWith:
        case t => errChan.raise(err)

    def orRaise(coerce: Throwable => Error) =
      fa.handleErrorWith:
        case t =>
          val err = coerce(t)
          error"$err" >> errChan.raise(err)


  given Encoder[Throwable]        = Encoder[String].contramap(_.toString)
  given Encoder[ServiceError]     = deriveEncoder
  given Encoder[InterpreterError] = deriveEncoder

  given Encoder[Error] =
    Encoder.instance:
      case s: ServiceError     => s.asJson
      case i: InterpreterError => i.asJson
