//> using scala 3.5.0
//> using dep co.fs2::fs2-core::3.10.2
//> using dep co.fs2::fs2-io::3.10.2
//> using dep co.fs2::fs2-scodec:3.10.2
//> using dep org.typelevel::cats-core:2.12.0
//> using dep org.typelevel::cats-effect::3.5.4
//> using dep org.typelevel::cats-mtl:1.4.0


package fs2test


import cats._
import cats.implicits._
import cats.effect._
import cats.effect.std._
import fs2._


object fs2test extends IOApp:

  //https://fs2.io/#/guide
  def run(args: List[String]) =
    for
      _   <- Console[IO].println("Hello fs2")
      tr   = new TestRunner[IO]

      msg <- tr.catchAndLog(tr.throws)
               .compile
               .drain

      rep  = tr.repeat(Stream(1, 0)).take(5)

      s1  <- rep.compile.toList
      _   <- Console[IO].println(s1)

      s2  <- tr.drain(rep)
      _   <- Console[IO].println(s2)

      s3  <- tr.exec(Console[IO].println(42))
      _   <- Console[IO].println(s3)

      // This stream fails fast, the second rep is never evaluated
      att  = tr.attempt(rep ++ Stream(3).flatMap(_ => tr.throws) ++ rep)

      s4  <- att.compile.toList
      _   <- Console[IO].println(s4)

      s5  <- rep.through(tr.takePipe(3)).compile.toList
      _   <- Console[IO].println(s5)

      s6  <- rep.through(tr.takePipePull(3)).compile.toList
      _   <- Console[IO].println(s6)

      s7  <- rep.through(tr.takePipeSimple(3)).compile.toList
      _   <- Console[IO].println(s7)

      s8  <- att
               .through(tr.takeWhile(_.isRight))
               .compile
               .toList

      _   <- Console[IO].println(s8)

      s9  <- Stream("a", "b", "c")
               .through(tr.intersperse("|"))
               .compile
               .toList

      _   <- Console[IO].println(s9)

      s10 <- Stream
               .range(1, 10)
               .through(tr.scan(0)(_ + _))
               .compile
               .toList

      _   <- Console[IO].println(s10)

    yield
      ExitCode.Success


final class TestRunner
  [F[_] : Concurrent : Console]:

  def throws =
    Stream.raiseError[F](new Exception("Launching missiles"))


  def catchAndLog(s: Stream[F, ?]) =
    s.handleErrorWith:
      case e => Stream.eval(Console[F].println(e.getMessage))


  def repeat(s: Stream[F, ?]): Stream[F, ?] =
    s ++ repeat(s)


  def drain(s: Stream[F, ?]) =
    s.compile.drain


  def exec[A](fa: F[A]) =
    drain(Stream.eval(fa))


  def attempt(s: Stream[F, ?]) =
    s.map(Right(_))
     .handleErrorWith:
       case e => Stream.emit(Left(e))


  def takePipe[O](n: Long): Pipe[F, O, O] =
    _.scanChunksOpt(n): n =>
      if n <= 0 then None
      else Some((c: Chunk[O]) => c.size match
        case m if m < n => (n - m, c)
        case _          => (0, c.take(n.toInt)))


  def takePipePull[O](n: Long): Pipe[F, O, O] =

    def go(s: Stream[F, O], n: Long): Pull[F, O, Unit] =
      s.pull.uncons.flatMap:
        case None         => Pull.done
        case Some(h -> t) =>
          h.size match
            case m if m <= n => Pull.output(h) >> go(t, n - m)
            case _           => Pull.output(h.take(n.toInt))

    go(_, n).stream


  def takePipeSimple[O](n: Long): Pipe[F, O, O] =
    _.pull.take(n).void.stream


  def takeWhile[O](f: O => Boolean): Pipe[F, O, O] =

    def go(s: Stream[F, O]): Pull[F, O, Unit] =
      s.pull.uncons.flatMap:
        case None         => Pull.done
        case Some(h -> t) =>
          h.indexWhere(!f(_)) match
            case Some(i) => Pull.output(h.take(i)) >> Pull.done
            case _       => Pull.output(h) >> go(t)

    go(_).stream


  def intersperse[O](sep: O): Pipe[F, O, O] =
    _.flatMap(Stream(sep, _)).drop(1)


  def scan[O, O2 >: O](o: O2)(f: (O2, O) => O2): Pipe[F, O, O2] =

    def go(z: O2, s: Stream[F, O]): Pull[F, O2, Unit] =
      s.pull.uncons.flatMap:
        case None         => Pull.done
        case Some(h -> t) =>
          val acc = h.scanLeft(z)(f)
          Pull.output(acc.drop(1)) >> go(acc.last.get, t)

    Stream(o) ++ go(o, _).stream

