//> using scala 3.4.0
//> using options -Werror
//> using dep info.umazalakain::errata:0.6.2
//> using dep org.typelevel::cats-core:2.10.0
//> using dep org.typelevel::cats-effect:3.5.4
//> using dep org.typelevel::cats-mtl:1.4.0


import cats._
import cats.data.EitherT
import cats.effect._
import cats.effect.std._
import cats.implicits._
import errata._
import errata.instances._
import errata.syntax._
import errata.syntax.all.HandleToSyntax


object Main extends IOApp:

  type Stack = [R] =>> EitherT[IO, Errors.Error, R]

  lazy val service = new Service[Stack]

  def run(args: List[String]) =
    for
      _   <- Console[IO].println("Yo")
      i   <- service.inputService
      r1  <- service.mathService(i._1, i._2, i._3)
      r2  <- service.mathService(i._2, i._3, i._1)
      r3  <- service.mathService(i._3, i._1, i._2)
      res  = s"$r1\n$r2\n$r3"
      _   <- Console[Stack].println(res).value
    yield
      ExitCode.Success


object Errors:

  type Channel = [F[_]] =>> errata.Raise[F, Error]
  type SinkIO  = [F[_]] =>> errata.ErrorsTo[F, IO, Error]

  type Error = Failure | ServiceError

  enum ServiceError:
    case Arithmetic(msg: String)
    case Generic(msg: String)

  enum Failure:
    case Fatal(msg: String, exitCode: Int)


final class Service[F[_]: Console : Errors.Channel : Monad]:

  lazy val errchan = summon[Errors.Channel[F]]

  def inputService(using Errors.SinkIO[F]) =
    serve(gatherInputs, (0, 0, 0))


  def mathService(x: Double, y: Double, z: Double)(using Errors.SinkIO[F]) =
    serve(doTheMath(x, y), z)


  private def serve[A](fa: F[A], default: A)(using Errors.SinkIO[F]) =
    fa.handleWith:
      // Extra match needed for totality
      case e: Errors.Error => e match
        case Errors.ServiceError.Arithmetic(msg) =>
          Console[IO].println(msg) >> default.pure[IO]
        case Errors.ServiceError.Generic(msg) =>
          Console[IO].println(msg) >> default.pure[IO]
        case Errors.Failure.Fatal(msg, _) =>
          Console[IO].println(msg) >> default.pure[IO]


  private def gatherInputs =
    for
      _ <- Console[F].println("3 inputs ->")
      x <- Console[F].readLine.map(_.toIntOption)
      y <- Console[F].readLine.map(_.toIntOption)
      z <- Console[F].readLine.map(_.toIntOption)
      r <- if x.isEmpty || y.isEmpty || z.isEmpty then errchan.raise(Errors.Failure.Fatal("Input is mega wrong!", -1)) 
           else (x.get, y.get, z.get).pure[F]
    yield
      r


  private def doTheMath(a: Double, b: Double) =

    def div(x: Double, y: Double) =
      if y == 0 then errchan.raise(Errors.ServiceError.Arithmetic("Div by 0!"))
      else (x  / y).pure[F]

    def root(x: Double) =
      if x < 0 then errchan.raise(Errors.ServiceError.Arithmetic("Negative root!"))
      else math.sqrt(x).pure[F]

    for
      d <- div(a, b)
      r <- root(d)
    yield
      r
