namespace java j.generated.data
namespace py   py.generated.data


struct Thing {
    1: required i64    id
    2: required string name
}


struct Result {
    1: required string msg
}
