include "data.thrift"

namespace java j.generated.services
namespace py   py.generated.services


service ThingProcessor {
    data.Result processThing(1: data.Thing theThing)
}
