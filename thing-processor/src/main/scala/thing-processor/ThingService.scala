package thingprocessor

import cats.*
import cats.implicits.*
import cats.effect.*
import cats.effect.implicits.*
import cats.effect.std.*
import org.apache.thrift.server.*
import org.apache.thrift.server.TThreadPoolServer.Args
import org.apache.thrift.transport.TServerSocket
import org.typelevel.log4cats.*
import org.typelevel.log4cats.slf4j.*
import org.typelevel.log4cats.syntax.*
import scala.concurrent.duration.DurationInt

import j.generated.data.*
import j.generated.services.*


final class ThingService[F[_] : Async](config: ThingServiceConfig):

  given Logger[F] = Slf4jLogger.getLogger[F]

  final class ThingHandler(d: Dispatcher[F]) extends ThingProcessor.Iface:
    def processThing(t: Thing) =
      d.unsafeRunSync:
        for
          _ <- Temporal[F].sleep(20.millis)
          r  = new Result
          _  = r.setMsg(s"Thing has id ${t.id} and name ${t.name}")
        yield
          r

  lazy val start =
    info"Starting" >>
    Dispatcher
      .parallel[F]
      .use: d =>
        val proc      = new ThingProcessor.Processor(new ThingHandler(d))
        val transport = new TServerSocket(config.port)
        val server    = new TThreadPoolServer(new Args(transport).processor(proc))
        Sync[F].interruptible(server.serve)
      .start


object ThingService:

  def apply[F[_] : Async](config: ThingServiceConfig):
    Resource[F, ThingService[F]] =
    Resource.pure(new ThingService(config))
