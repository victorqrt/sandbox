package thingprocessor

import cats.*
import cats.effect.*


object Main extends IOApp.Simple:

  val conf = Config(ThingServiceConfig(8998))

  val thingProcessor =
    for
      service <- ThingService[IO](conf.thingServiceConfig)
      client1 <- ThingClient[IO](conf.thingServiceConfig)
      client2 <- ThingClient[IO](conf.thingServiceConfig)
    yield
      (service, client1, client2)

  val run =
    thingProcessor
      .use:
        case (s, c1, c2) =>
          s.start >>
          c1.start >>
          c2.start >>
          IO.never
