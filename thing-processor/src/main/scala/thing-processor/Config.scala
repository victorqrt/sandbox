package thingprocessor


case class ThingServiceConfig
  ( port: Int )


case class Config
  ( thingServiceConfig: ThingServiceConfig )
