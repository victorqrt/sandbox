package thingprocessor

import cats.*
import cats.implicits.*
import cats.effect.*
import cats.effect.implicits.*
import org.apache.thrift.protocol.TBinaryProtocol
import org.apache.thrift.transport.TSocket
import org.typelevel.log4cats.*
import org.typelevel.log4cats.slf4j.*
import org.typelevel.log4cats.syntax.*
import scala.concurrent.duration.DurationInt

import j.generated.data.*
import j.generated.services.*


final class ThingClient[F[_] : Async](conf: ThingServiceConfig):

  given Logger[F] = Slf4jLogger.getLogger[F]

  private lazy val client =
    val transport = new TSocket("localhost", conf.port)
    transport.open
    new ThingProcessor.Client(
      new TBinaryProtocol(transport))

  private val rand = new scala.util.Random

  private def sendThing =
    for
      t   <- (new Thing).pure[F]
      _    = t.setId(rand.nextInt % 999)
      _    = t.setName(t.getId.toString)
      _   <- info"Sending thing $t"
      res <- Sync[F].blocking(client.processThing(t))
      _   <- info"Got a result: $res"
    yield
      res

  lazy val start =
    (Temporal[F].sleep(1.second) >> sendThing)
      .foreverM
      .start


object ThingClient:

  def apply[F[_] : Async](config: ThingServiceConfig):
    Resource[F, ThingClient[F]] =
    Resource.pure(new ThingClient(config))
