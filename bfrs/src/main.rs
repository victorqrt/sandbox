use std::cmp;
use std::env;
use std::fs;
use std::io;
use std::io::Read;
use std::io::Write;


struct BFMem<'a> {
	mem: &'a mut Vec<u8>,
	ptr: usize
}


impl BFMem<'_> {

	fn check_realloc(&mut self) {
		if self.mem.len() <= self.ptr {
			self.mem.extend(vec![0; self.ptr + 1]);
		}
	}

	fn get(&self) -> u8 {
		if self.mem.len() <= self.ptr { 0 }
		else { self.mem[self.ptr] }
	}

	fn set(&mut self, byte: u8) {
		self.check_realloc();
		self.mem[self.ptr] = byte;
	}

	fn add(&mut self, byte: i8) {
		let current = self.get();
		self.set((current as isize + byte as isize) as u8);
	}

	fn shift(&mut self, offset: isize) {
		self.ptr = cmp::max((self.ptr as isize + offset).try_into().unwrap(), 0);
	}
}


fn main() {

	let args: Vec<_> = env::args().collect();

	if args.len() < 2 {
		panic!("Usage: bfrs <script>");
	}

	let src         = fs::read_to_string(args[1].as_str()).unwrap();
	let ops: Vec<_> = src.chars().collect();
	let mut mem     = BFMem { mem: &mut vec![0, 64], ptr: 0 };
	let mut isp     = 0;
	let mut loopcnt = 0;

	loop {

		if isp >= ops.len() {
			return;
		}

		let mut op = ops[isp];

		match op {
			'+' => mem.add(1),
			'-' => mem.add(-1),
			'>' => mem.shift(1),
			'<' => mem.shift(-1),

			'.' => {
				print!("{}", mem.get() as char);
				if let Err(_e) = io::stdout().flush() { }
			},

			',' => {
				let input = io::stdin()
					.bytes()
					.next()
					.and_then(|r| r.ok())
					.unwrap();

				mem.set(input);
			},

			'[' => {
				if mem.get() == 0 {
					isp += 1;
					loop {
						op = ops[isp];

						if op == ']' && loopcnt == 0 {
							break;
						}

						else if op == ']' {
							loopcnt -= 1;
						}

						else if op == '[' {
							loopcnt += 1;
						}

						isp += 1;
					}
				}
			},

			']' => {
				if mem.get() != 0 {
					isp -= 1;
					loop {
						op = ops[isp];

						if op == '[' && loopcnt == 0 {
							break;
						}

						else if op == '[' {
							loopcnt -= 1;
						}

						else if op == ']' {
							loopcnt += 1;
						}

						isp -= 1;
					}
				}
			},

			_   => { }
		}

		isp += 1;
	}
}
