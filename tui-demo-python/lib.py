import logging, requests, sys

from flask import Flask, make_response

from textual.app import App
from textual.containers import Horizontal
from textual.widgets import Button, Header, Footer


class KVStore:

    app   = Flask(__name__)
    store = { }

    @app.route("/", methods=["GET"])
    def all():
        logging.info("Getting all")
        return KVStore.store

    @app.route("/<key>", methods=["GET"])
    def get(key):
        val = KVStore.store.get(key)
        logging.info(f"Getting {key} -> {val}")
        return val if val is not None else "None"

    @app.route("/<key>/<val>", methods=["PUT"])
    def put(key, val):
        logging.info(f"Putting {key} -> {val}")
        KVStore.store[key] = val
        return "OK"


class Tui(App):

    def __init__(self):
        super().__init__()
        self.server = "http://localhost:5000"
        self.btn    = Button("None")

    def compose(self):
        yield Header()
        yield Horizontal(self.btn)
        yield Footer()

    def on_button_pressed(self, event):
        self.btn.label = requests.get(f"{self.server}/label").text


if __name__ == "__main__":

    if "server" in sys.argv:
        KVStore.app.run()
    else:
        Tui().run()
