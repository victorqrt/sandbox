//> using dep io.github.irevive::union-derivation-core:0.0.4
//> using dep io.circe::circe-generic:0.14.5

import io.circe._
import io.circe.generic.semiauto._
import io.circe.syntax._
import io.github.irevive.union.derivation._
import scala.compiletime.{erasedValue, summonInline}
import scala.deriving._


object EncoderDerivation:

  
  private def encodeA[A](a: A, enc: Encoder[?]) =
    enc.asInstanceOf[Encoder[A]](a)


  private def sumEncoder[A : Mirror.SumOf](encs: => List[Encoder[?]]): Encoder[A] =
    val sum = summon[Mirror.SumOf[A]]
    new Encoder[A]:
      final def apply(a: A) = encodeA(a, encs(sum ordinal a))


  private def productEncoder[A : Mirror.ProductOf](encs: => List[Encoder[?]]): Encoder[A] =
    val product = summon[Mirror.ProductOf[A]]
    new Encoder[A]:
      final def apply(a: A) =
        val prodA = a.asInstanceOf[Product]

        prodA
          .productIterator
          .zip(prodA.productElementNames)
          .zip(encs.iterator)
          .map:
            case name -> field -> enc =>
              Json.obj(name.toString -> encodeA(field, enc))
          .reduce(_ deepMerge _)


  inline def summonAll[A <: Tuple]: List[Encoder[?]] =
    inline erasedValue[A] match
      case _: EmptyTuple => Nil
      case _: (t *: ts)  => summonInline[Encoder[t]] :: summonAll[ts]


  inline given derived[A : Mirror.Of]: Encoder[A] =
    val mirror    = summon[Mirror.Of[A]]
    val instances = summonAll[mirror.MirroredElemTypes]
    inline mirror match
      case p: Mirror.ProductOf[A] => productEncoder(instances)(using p)
      case s: Mirror.SumOf[A]     => sumEncoder(instances)(using s)

  
  inline def deriveForUnion[For[_], A : IsUnion]: For[A] =
    UnionDerivation.derive[For, A]


object Data:

  enum Odd:
    case One, Three

  enum Even:
    case Two, Four

  export Even._
  export Odd._

  type IntNum = Even | Odd


object Codec:

  import Data._
  import EncoderDerivation._

  given Encoder[Even] = deriveEncoder
  given Encoder[Odd]  = deriveEncoder

  given Encoder[IntNum] = deriveForUnion[Encoder, IntNum]


@main
def main =
  import Data._
  import Codec._
  //println(One.asJson)
  println(One)
