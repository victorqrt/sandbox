//> using scala 3.4.2
//> using dep "org.scala-lang.modules::scala-parser-combinators:2.4.0"

import scala.util.parsing.combinator._


object JsonParser extends JavaTokenParsers:

  type JsonObject = Map[String, ?]
  type JsonValue  = List[?] | JsonObject | Boolean | Double | String | Unit
  type P          = Parser[JsonValue]

  def key       = stringLiteral
  def num:    P = floatingPointNumber ^^ { _.toDouble }
  def strVal: P = stringLiteral
  def jnull:  P = "null" ^^^ ()
  def arr:    P = "[" ~> repsep(value, ",") <~ "]"
  def bool:   P = ("true" | "false") ^^ { _ == "true" }
  def value:  P = arr | bool | jnull | strVal | num | obj
  def obj:    P = "{" ~> repsep(assignment, ",") <~ "}" ^^ { _.toMap }

  def assignment: Parser[(String, JsonValue)] =
    key ~ ":" ~ value ^^ { case k ~ ":" ~ v => k -> v }

  def apply(str: String) = parseAll(value, str)


object JsonTests extends App:

  import java.nio.file.{Files, Paths}
  import scala.io.Source
  import scala.jdk.StreamConverters._
  import scala.math._
  import scala.util.Random
  import JsonParser._

  Files
    .find(Paths.get("test"), 1,
      (p, _) => p.getFileName.toString.endsWith(".json"))
    .toScala(List)
    .map(_.toString)
    .map(s => s"$s: ${JsonParser(Source.fromFile(s).mkString)}")
    .foreach(println)
