//> using scala 3.5.0


object Board:

  opaque type PieceCoord = (Char, Int, Int)


  inline def makeRankStr(rank: Int, pieces: Seq[PieceCoord]) =
    val base   = " |■| |■| |■| |■| "
    val skewed = if rank % 2 == 0 then base.drop(2) else base.dropRight(2)

    pieces
      .filter(_._2 == rank - 1)
      .foldLeft(skewed):
        case acc -> (c, _, x) => acc.updated(x * 2, c)


  class Position
    ( val fenStr:      String
    , val whiteToMove: Boolean
    , val pieces:      Seq[PieceCoord]):

    lazy val asStr =
      val vec =
        for rank <- 1 to 8
        yield s"$rank |${makeRankStr(rank, pieces)}|"

      val top  = "_" * 17
      val bot  = "-" * 17
      val rows = vec.reverse.reduce(_ + "\n" + _)
      val move = if whiteToMove then "White" else "Black"

      s"  $move to move\n  Board:\n  $top\n$rows\n  $bot\n   a b c d e f g h"


  object Position:

    val pieceMapper = Map
      ( 'k' -> '♚'
      , 'q' -> '♛'
      , 'r' -> '♜'
      , 'b' -> '♝'
      , 'n' -> '♞'
      , 'p' -> '♟'
      , 'K' -> '♔'
      , 'Q' -> '♕'
      , 'R' -> '♖'
      , 'B' -> '♗'
      , 'N' -> '♘'
      , 'P' -> '♙'
      )

    def apply(fen: String) =

      val Array(board, move, castling, enPassant, hm50, fullMove) =
        fen.split(' ')

      new Position(fen, move(0) == 'w',
        board
          .split('/')
          .zipWithIndex
          .flatMap:
            case rank -> rankIdx =>
              rank
                .foldLeft(0 -> Seq[PieceCoord]()):
                  case idx -> acc -> next =>
                    if next.isDigit then (idx + next.asDigit, acc)
                    else
                      val piece = pieceMapper.getOrElse(next, '?')
                      (idx + 1, acc :+ (piece, rankIdx, idx))
                ._2
          .toIndexedSeq)


object FenParserApp extends App:

  io.Source
    .fromFile("fens.txt")
    .getLines
    .foreach(fen => println(s"${Board.Position(fen).asStr}\n"))
