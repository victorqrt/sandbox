// https://doc.rust-lang.org/book/

fn main() {

    let s = String::from("Hello, world");
    //let mut s = String::from("Hello, world");

    // Shadows s, passes ownership back and forth
    let (s, _) = sz_owning_and_returning(s);

    // Borrow (non-owning reference, immutable here)
    let sz = sz_borrowing_ref(&s);
    println!("{} of size {}", s, sz);

    let fw = first_word(&s);
    //s.clear();
    // try and make s mut (`let mut s...` on top of main) to allow for clear()
    // fw is a slice of the heap region backing s, so there is a (immutable) borrow
    // clear performs a mutable borrow, so will not compile.
    // move it below last usage of fw and it's all good
    println!("First word is {}", fw);

    let stuff = test_struct_moves();
    println!("{} -> {}", stuff.id, stuff.name);

    test_patmat();

    test_option_compose();

    test_vecs();

    test_str_quirks();

    test_hashmap();

    test_err_handling();

    test_generics();

    test_lifetimes();

    test_grep();

    test_closures();

    test_iterators();

    test_smart_pointers();

    test_refcycles();

    test_concurrency();

    test_oop();

    test_trait_objects();

    test_oo_pattern();

    test_raw_ptrs();
}


fn sz_owning_and_returning(s: String) -> (String, usize) {

    // Owning and returning the param like this is not practical
    // But shows some of the borrow checker behavior
    //
    // Can't return (s, s.len()), temporary is needed here
    // This is because the tuple is "moved" in order:
    // when trying to evaluate s.len s is already returned (moved out)
    // hence invalid borrow-after-move (from String::len)
    //
    // If you change the example from the book to return (usize, String)
    // then you could write it as a single expression

    let sz = s.len();
    (s, sz)
}


fn sz_borrowing_ref(s: &String) -> usize { s.len() }


//fn first_word(s: &String) -> &str {
// &String is coerced into &str
fn first_word(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (idx, &byte) in bytes.iter().enumerate() {
        if byte == b' ' {
            return &s[..idx];
        }
    }

    &s[..]
}


struct Stuff { id: usize, name: String }


impl Stuff {

    fn show(&self) -> String {
        let mut showed = self.name.clone();
        showed.push_str(" has id ");
        showed.push_str(&self.id.to_string());
        showed
    }

    fn new(id: usize, name: &str) -> Self {
        Self { id, name: String::from(name) }
    }
}


//impl Drop for Stuff {
//    fn drop(&mut self) {
//        println!("Dropping the stuff! {} -> {}", self.id, self.name)
//    }
//}


fn test_struct_moves() -> Stuff {

    let foo = Stuff::new(1, "theString");
    println!("{}", foo.show());

    let bar = Stuff { name: String::from("str2"), ..foo };

    // if we implement Drop for Stuff, does not compile
    // we are moving the String (which is Drop) out of foo
    let _baz = Stuff { id: 2, ..foo };

    println!("{}", foo.id);
    //println!("{}", foo.name);
    // Does not compile as name was moved out of foo into _baz, can't borrow
    // Not the case for id as it was copied not moved / borrowed

    //foo
    // Does not compile as we moved name out of foo ("partial move") so can't
    // use foo as a whole

    println!("{}", bar.show());
    bar
    // All fine and dandy as id is usize, Copy not Drop so no move when using ..foo syntax above
    // We could also return _baz which now owns the String from foo
}


// Enums can hold fields
enum TestEnum {
    Empty,
    Tup(isize, usize),
    NamedFields { s: String, u: usize }
}


impl TestEnum {

    //fn new() -> Self { Self::Empty }
    //
    //fn new(i: isize, u: usize) -> Self ....
    //
    //.....
    //
    // Can't overload `new` to have "smart constructors"
    // Need to refer to the enum ctor

    // Returning &str here would require specifying a lifetime
    // Owned String is moved out instead
    fn show(&self) -> String {
        match self {
            Self::Empty                => "Empty".to_string(),
            Self::Tup(i, u)            => format!("Tuple of {i} and {u}"),
            Self::NamedFields { s, u } => format!("NF of {s} and {u}")
        }
    }
}


fn test_patmat() {
    let e = TestEnum::Tup(0, 2);
    println!("{}", e.show());

    let e = TestEnum::NamedFields { s: "Ah".to_string(), u: 42 };
    println!("{}", e.show());

    let e = TestEnum::Empty;
    println!("{}", e.show());
}


fn test_option_compose() {

    fn safe_root(d: f64) -> Option<f64> {
        if d < 0.0 { None }
        else       { Some(d.sqrt()) }
    }

    fn safe_inverse(d: f64) -> Option<f64> {
        if d == 0.0 { None }
        else        { Some(1.0 / d) }
    }

    fn safe_root_of_inverse(d: f64) -> Option<f64> {
        safe_inverse(d).and_then(safe_root)
    }

    println!("{:?}", safe_root_of_inverse(42.0));
    println!("{:?}", safe_root_of_inverse(-4.0));
    println!("{:?}", safe_root_of_inverse(0.0));

    let d = 0.0;

    if let Some(inv) = safe_root_of_inverse(d) {
        println!("{} has inv_root {}", d, inv);
    }

    else {
        println!("{} has no inv_root", d);
    }
}


struct Thing { id: usize }


impl Drop for Thing {
    fn drop(&mut self) {
        println!("Dropping a thing! ({})", self.id);
    }
}


fn test_vecs() {
    // Immutable here, inferred as Vec<i32>
    let v = vec![1, 2, 3];

    //let head: &i32 = &v[0];
    // ^ panics the program if bounds checking fails

    let head: Option<&i32> = v.get(0);

    match head {
        Some(i) => println!("Head of the vec is {i}"),
        None    => println!("Empty vec")
    }

    // Same as with &str slices in the first_word example
    // Here Vec::push may need to reallocate and move values around
    // So it has to take a mutable borrow, hence the following won't compile
    //v.push(4);

    // As v gets Dropped, so do its elements
    let mut v = Vec::<Thing>::new();
    v.push(Thing { id: 1 });
    v.push(Thing { id: 2 });

    for i in &v {
        print!("{} ", i.id);
    }

    println!("");

    // Things get dropped
}


fn test_str_quirks() {
    let hello = "Здравствуйте"; // &str (actually &'static str)

    //let c1 = &hello[0];
    // does not compile: you CANNOT index &str with integers
    // there is no guarantee that a byte-by-byte slice of memory is valid UTF8
    // but any &str is GUARANTEED to hold valid UTF8

    //let first_chars = &hello[0..4];
    // Compiles and works, because the first 4 bytes here slice "between" UTF8 "chars"
    // here the slice holds "Зд"

    // BUT
    //let first_chars = &hello[0..1];
    // Compiles and... we are dead! Runtime panic as we are slicing inside an UTF8 char
    // So we cannot index &str safely, but we can still iterate: we need to consider 2 ways of
    // doing so - over bytes OR over chars (goodbye C assumptions)
    // Below code prints 12 chars, but 24 bytes

    for c in hello.chars() {
        print!("{c}");
    }

    println!("");

    for b in hello.bytes() {
        print!("{b} ");
    }

    println!("");

    let mut s = String::from("Hello");
    s.push(' '); // char

    let s2 = String::from("again!");
    let s  = s + &s2; // s is shadowed + moved into immutable binding
                      // + on String takes &str rhs, not &String but compiler coerces it into &str

    println!("{s}");
}


fn test_hashmap() {
    use std::collections::HashMap;

    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    let team_name = String::from("Blue");

    let score = scores
        .get(&team_name)
        .unwrap_or(&0);
        // OR
        //.copied()       <- to get i32 i/o &i32
        //.unwrap_or(0);  <- as now we are on Option<i32>
        //
        // Note i32s are Copy, so copied on insert any way
        // The keys are String and therefore moved

    println!("{team_name} score: {score}");

    for (k, v) in &scores {
        println!("{k} -> {v}");
    }

    // These are owned by the map after insertion
    let key = String::from("the_key");
    let val = String::from("the_val");

    let mut the_map = HashMap::new();
    the_map.insert(key, val);

    // Nope! Compiler will suggest to .clone() both of these on insert
    //println!("{key}");
    //println!("{val}");

    the_map
        .entry(String::from("the_key"))
        .or_insert(String::from("other_val"));

    the_map
        .entry(String::from("missing_key"))
        .or_insert(String::from("yet_another_val"));

    println!("{:?}", the_map);

    let text     = "hello world wonderful world";
    let mut occs = HashMap::new();

    for word in text.split_whitespace() {
        let count = occs
            .entry(word)
            .or_insert(0);

        *count += 1;
    }

    println!("{:?}", occs);

    // Summary section exercises

    // 1
    let mut ints   = vec![1, 3, 3, 7, 0, 4, 2, 0, 22, 0, 451, 13];
    let mut counts = HashMap::new();

    ints.sort();
    println!("Median: {}", &ints[ints.len() / 2]);

    for i in &ints {
        let entry = counts
            .entry(i)
            .or_insert(0);

        *entry += 1;
    }

    if let Some(m) = counts
        .iter()
        //.max_by(|e1, e2| e1.1.cmp(&e2.1))
        .max_by_key(|e| e.1)
        .map(|(k, _)| k) {

        println!("Mode: {m}");
    }

    // 2
    let mut s1 = String::from("first");
    let mut s2 = String::from("apple");

    fn ex2(s: &mut String) {
        let vowels = vec!['a', 'e', 'i', 'o', 'u', 'y'];

        let c = s.remove(0);

        if vowels.contains(&c) {
            *s = format!("{}{}-hay", c, s);
        }

        else {
            *s = format!("{}-{}ay", s, c);
        }
    }

    ex2(&mut s1);
    ex2(&mut s2);
    println!("{}, {}", s1, s2);

    // 3:
    let mut the_map = HashMap::<String, Vec<String>>::new();

    fn ex3(command: &str, map: &mut HashMap<String, Vec<String>>) {

        let mut parts = command.split_whitespace();
        let     name  = parts.nth(1);
        let     dept  = parts.nth(1);
        // .nth mutates the SplitWhitespace (advances the iterator)
        // hence the mut above

        if let (Some(name), Some(dept)) = (name, dept) {

            let dept_entry = map
                .entry(dept.to_string())
                .or_insert(Vec::<String>::new());

            dept_entry.push(name.to_string());
        }

        else {
            panic!("Wrong command: {}", command);
        }
    }

    ex3("Add Amir to Sales", &mut the_map);
    ex3("Add Sophie to Sales", &mut the_map);
    ex3("Add Sally to Engineering", &mut the_map);

    println!("{:?}", the_map);
}


fn test_err_handling() {

    use std::fmt;
    use std::error::Error;

    #[derive(Debug)]
    enum MyError {
        DivByZero,
        DomainError(String)
    }

    impl fmt::Display for MyError {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            let msg = match self {
                MyError::DivByZero      => "Dividing by zero!",
                MyError::DomainError(s) => &s
            };

            write!(f, "{}", msg)
        }
    }

    impl Error for MyError { }

    // Same example as with Option earlier

    type MyResult = Result<f64, MyError>;

    fn safe_inverse(x: f64) -> MyResult {
        if x == 0.0 { return Err(MyError::DivByZero); }
        Ok(1.0 / x)
    }

    fn safe_root(x: f64) -> MyResult {
        if x < 0.0 {
            return Err(MyError::DomainError(
                "Taking root of negative number!".to_string()));
        }

        Ok(x.sqrt())
    }

    fn safe_inv_root(x: f64) -> MyResult {
        let root = safe_root(x)?;
        safe_inverse(root)
    }

    println!("{:?}", safe_inv_root(9.0));
    println!("{:?}", safe_inv_root(-9.0));
    println!("{:?}", safe_inv_root(0.0));
}


fn test_generics() {

    // Syntax:
    // fn max<T: PartialOrd>(ts: &[T]) -> &T
    // not the same as e.g.
    // fn max(ts: &[impl PartialOrd + Display]) -> &(impl PartialOrd + Display)
    //
    // For arbitrary number of constraints prefer the below

    fn max<T>(ts: &[T]) -> &T
    where
        T: PartialOrd {

        let mut themax = &ts[0];

        for t in ts {
            if t > themax {
                themax = t;
            }
        }

        themax
    }

    let is = vec![1, 99, 0, 45, 2, 29, 11];
    let cs = vec!['h', 'e', 'l', 'z', 'o'];

    println!("{}", max(&is));
    println!("{}", max(&cs));
}


fn test_lifetimes() {

    fn longest<'a>(s1: &'a str, s2: &'a str) -> &'a str {
        if s1.len() > s2.len() { s1 }
        else                   { s2 }
    }

    let s1 = "short";
    let long;

    {
        let s2 = "a bit longer"; // &'static str
        long   = longest(s1, s2);

        //let s2 = "a bit longer".to_owned(); // owned String
        //long   = longest(s1, s2.as_str());

        // Compiles in both cases as owned String wasnt Dropped yet
        println!("From inner scope: the longest is \"{}\"", long);
    }

    // Fails to compile if we use the owned String, as it was dropped, borrow-after-move
    println!("The longest is \"{}\"", long);

    struct HoldsReference<'a> {
        the_ref: &'a str
    }

    impl HoldsReference<'_> {
        // Returned reference inferred with lifetime of &self as per elision rules
        fn get_it(&self) -> &str {
            self.the_ref
        }
    }

    let holds;

    {
        let owned   = s1.to_owned();
        let the_ref = owned.as_str();
        holds       = HoldsReference { the_ref };

        println!("{}", holds.get_it());
    }

    // Certainly not
    //println!("{}", holds.get_it());
}


#[cfg(test)]
mod tests {

    #[test]
    fn the_test() {
        assert!(true);
        assert_eq!(6 * 6, 36);
        assert_ne!(11 + 99, 100);
    }

    #[test]
    #[should_panic(expected = "aaaah")]
    fn panics() {
        panic!("aaaah");
    }

    #[test]
    fn chained_results() -> Result<(), String> {

        fn inner(i: isize) -> Result<(), String> {
            if i == 0 { Ok(()) }
            else      { Err(format!("Not {}...", i)) }
        }

        // ? to short-circuit and fail fast, Result value for the test result
        //inner(2)?;
        inner(0)?;
        inner(0)
    }

    // Some cargo test features explained: https://doc.rust-lang.org/book/ch11-02-running-tests.html
}


fn test_grep() {

    use std::env;
    use std::error::Error;
    use std::fs;
    use std::process;

    //let args: Vec<String> = env::args().collect();
    //dbg!(args);

    // Slightly different than in the book, to avoid cloning
    struct Config<'a> {
        query:       &'a str,
        path:        &'a str,
        ignore_case: bool
    }

    impl Config<'_> {
        fn new(args: &[String]) -> Result<Config, &'static str> {
            if args.len() < 3 {
                return Err("Not enough args");
            }

            let query       = args[1].as_str();
            let path        = args[2].as_str();
            let ignore_case = env::var("IGNORE_CASE").is_ok();

            Ok(Config { query, path, ignore_case })
        }
    }

    let args: Vec<String> = vec!["rust-sandbox", "grep", "src/main.rs"]
        .iter()
        .map(|&s| s.to_owned())
        .collect();

    let conf = Config::new(&args).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {err}");
        process::exit(1);
    });

    fn run_grep(conf: Config) -> Result<(), Box<dyn Error>> {

        fn search<'a>(query: &str, contents: &'a str, ignore_case: bool) -> Vec<&'a str> {
            contents
                .lines()
                .filter(|line| {
                    if ignore_case {
                        line
                            .to_lowercase()
                            .contains(&query.to_lowercase())
                    }

                    else {
                        line.contains(query)
                    }

                })
                .collect()
        }

        println!("Searching for '{}' in {}...", conf.query, conf.path);

        let contents = fs::read_to_string(conf.path)?;
        let results  = search(conf.query, &contents, conf.ignore_case);
        // ignore_case = true matches GrEp
        println!("  => {} lines matched: {:?}", results.len(), results);

        Ok(())
    }

    if let Err(e) = run_grep(conf) {
        eprintln!("App error: {e}");
        process::exit(1);
    }
}


fn test_closures() {
    let mut vec = vec![1, 2, 3];
    println!("{:?}", vec);

    // Mutable binding required here as .push borrows captured environment mutably
    let mut push_it = || vec.push(4);

    // Nope, immutable borrow while we have a mutable one (into the closure object)
    //println!("{:?}", vec);

    push_it();
    // Ok now, closure unused from this point on, end of borrow
    println!("{:?}", vec);
}


fn test_iterators() {
    let vec = (1..99).collect();

    let any_divides = |x: usize, divs: &Vec<usize>| divs.iter().any(|d| x % d == 0);

    // Memoized primes
    let mut known_primes = vec![2];

    // FnMut, captures known_primes mutably
    let mut primes_in = |xs: Vec<usize>| -> Vec<usize> {
        xs
            .into_iter()
            .filter(|&x| x >= 2)
            .filter(|&x| {
                let candidate_divs: Vec<_> = known_primes
                    .iter()
                    .map(|&p| p)
                    .filter(|&p| p <= (x as f64).sqrt().floor() as usize)
                    .collect();

                if !any_divides(x, &candidate_divs) {
                    known_primes.push(x);
                    return true;
                }

                false
            })
            .collect()
    };

    // Nope, mutable-capturing closure still alive (used below)
    //println!("{:?}", known_primes);

    let theprimes = primes_in(vec);
    println!("{:?}", theprimes);

    // Fine
    //println!("{:?}", known_primes);
}


fn test_smart_pointers() {

    // Box

    #[derive(Debug)]
    enum List<T> {
        Cons(T, Box<List<T>>),
        Nil
    }

    use List::*;

    let list = Cons(1, Box::new(Cons(2, Box::new(Cons(3, Box::new(Nil))))));

    println!("{:?}", list);

    // Deref

    use std::fmt::Debug;

    struct MyBox<T: Debug>(T);

    impl<T: Debug> MyBox<T> {
        fn new(t: T) -> MyBox<T> {
            MyBox(t)
        }
    }

    use std::ops::{Deref, DerefMut};

    impl<T: Debug> Deref for MyBox<T> {
        type Target = T;

        fn deref(&self) -> &Self::Target {
            // return a regular & reference otherwise value would be moved out
            &self.0
        }
    }

    let mb = MyBox::new(42);
    assert_eq!(42, *mb);

    // Above actually calls *(mb.deref())
    // More about &String -> &str coercion: String implements Deref, and returns &str
    // Deref coercion in general will go "all the way" to the value
    // example:

    let takes_slice = |s: &str| println!("{s} partner");
    let boxed_str   = MyBox::new(String::from("Howdy"));
    takes_slice(&boxed_str);

    // ^ Here we get MyBox::deref giving back &String, then again String::deref for &str
    // Without Deref coercion we'd need to pass &(*boxed_str)[..]

    // Mutability example
    impl<T: Debug> DerefMut for MyBox<T> {
        fn deref_mut(&mut self) -> &mut Self::Target {
            &mut self.0
        }
    }

    let mut the_thing = MyBox::new(String::from("Greetings"));
    let changes_str   = |s: &mut String| s.push_str(" dude");

    changes_str(&mut the_thing);
    println!("{}", *the_thing);

    // Drop

    impl<T: Debug> Drop for MyBox<T> {
        fn drop(&mut self) {
            println!("Droppy time: {:?}", self.0);
        }
    }

    // Manually using std::mem::drop, or automatically at the end of scope
    drop(mb);

    // Rc
    use std::rc::Rc;

    #[derive(Debug)]
    enum RcList<T> {
        Cons(T, Rc<RcList<T>>),
        Nil
    }

    let tail = Rc::new(RcList::Cons(2, Rc::new(RcList::Cons(3, Rc::new(RcList::Nil)))));
    assert_eq!(Rc::strong_count(&tail), 1);

    let l0 = RcList::Cons(0, Rc::clone(&tail));

    {
        let _l1 = RcList::Cons(1, Rc::clone(&tail));
        assert_eq!(Rc::strong_count(&tail), 3);
    }

    assert_eq!(Rc::strong_count(&tail), 2);
    drop(l0);
    assert_eq!(Rc::strong_count(&tail), 1);

    use std::cell::RefCell;

    // RefCell (interior mutability - enforces borrowing rules at runtime, panicking on violation)
    {
        pub trait Messenger {
            fn send(&self, msg: &str);
        }

        pub struct LimitTracker<'a, T: Messenger> {
            messenger: &'a T,
            value:     usize,
            max:       usize
        }

        impl<'a, T: Messenger> LimitTracker<'a, T> {
            pub fn new(messenger: &'a T, max: usize) -> LimitTracker<'a, T> {
                LimitTracker { messenger, value: 0, max }
            }

            pub fn set_value(&mut self, value: usize) {
                self.value  = value;
                let portion = self.value as f64 / self.max as f64;

                if portion >= 1.0 {
                    self.messenger.send("Error: going over limit");
                }

                else if portion >= 0.9 {
                    self.messenger.send("Warning: 90% of limit");
                }

                else if portion >= 0.75 {
                    self.messenger.send("Warning: 75% of limt");
                }
            }
        }

        pub struct MockMessenger {
            sent: RefCell<Vec<String>>
        }

        impl MockMessenger {
            fn new() -> MockMessenger {
                MockMessenger { sent: RefCell::new(vec![]) }
            }
        }

        impl Messenger for MockMessenger {
            fn send(&self, msg: &str) {
                self
                    .sent
                    .borrow_mut()
                    .push(msg.to_owned());

                // below compiles but panics at runtime
                // thread 'main' panicked at 'already borrowed: BorrowMutError', src\main.rs:906:40
                // let mut b1 = self.sent.borrow_mut();
                // let mut b2 = self.sent.borrow_mut();
            }
        }

        let mock        = MockMessenger::new();
        let mut tracker = LimitTracker::new(&mock, 100);
        let the_len     = || tracker.messenger.sent.borrow().len();

        tracker.set_value(20);
        assert_eq!(the_len(), 0);
        tracker.set_value(80);
        assert_eq!(the_len(), 1);
        tracker.set_value(91);
        assert_eq!(the_len(), 2);
    }

    // Rc + RefCell (multiple ownership, interior mutability)
    {
        #[derive(Debug)]
        enum ListAgain<T> {
            Cons(Rc<RefCell<T>>, Rc<ListAgain<T>>),
            Nil
        }

        let value = Rc::new(RefCell::new(999));
        let tail  = Rc::new(ListAgain::Cons(Rc::clone(&value), Rc::new(ListAgain::Nil)));
        let l0    = ListAgain::Cons(Rc::new(RefCell::new(0)), Rc::clone(&tail));
        let l1    = ListAgain::Cons(Rc::new(RefCell::new(1)), Rc::clone(&tail));

        println!("l0 before: {:?}", l0);
        println!("l1 before: {:?}", l1);

        // borrow_mut gives a RefMut<T>, which we dereference with *
        // 'value' is a Rc, but gets auto-Deref'd into RefCell by . notation
        *value.borrow_mut() += 1;

        println!("l0 after: {:?}", l0);
        println!("l1 after: {:?}", l1);
    }
}


fn test_refcycles() {

    use std::cell::RefCell;
    use std::rc::{Rc, Weak};

    #[derive(Debug)]
    enum List<T> {
        Cons(T, RefCell<Rc<List<T>>>),
        Nil
    }

    use List::*;

    impl<T> List<T> {
        fn tail(&self) -> Option<&RefCell<Rc<List<T>>>> {
            match self {
                Cons(_, t) => Some(t),
                Nil        => None
            }
        }
    }

    let l = Rc::new(Cons(5, RefCell::new(Rc::new(Nil))));
    println!("Inital refcount: {}", Rc::strong_count(&l));
    println!("Tail: {:?}", l.tail());

    let l2 = Rc::new(Cons(10, RefCell::new(Rc::clone(&l))));
    println!("l2 inital refcount: {}", Rc::strong_count(&l2));
    println!("l refcount after l2 creation: {}", Rc::strong_count(&l));
    println!("l2 tail: {:?}", l2.tail());

    // Creating a cycle and leaking l + l2 (refcount never goes < 1)
    if let Some(t) = l.tail() {
        println!("Creating a cycle");
        *t.borrow_mut() = Rc::clone(&l2);
    }

    println!("l refcount: {}", Rc::strong_count(&l));
    println!("l2 refcount: {}", Rc::strong_count(&l2));
    // Stack overflow
    //println!("Tail: {:?}", l.tail());

    // Using Weak (non-owning) to avoid cycles through ref to parent

    #[allow(dead_code)]
    #[derive(Debug)]
    struct Node<T> {
        value:    T,
        parent:   RefCell<Weak<Node<T>>>,
        children: RefCell<Vec<Rc<Node<T>>>>
    }

    let show_refcounts = |node: &Rc<Node<usize>>|
        println!("Strong refcount = {}, weak = {}", Rc::strong_count(node), Rc::weak_count(node));

    let leaf = Rc::new(Node {
        value:    3,
        parent:   RefCell::new(Weak::new()),
        children: RefCell::new(vec![])
    });

    show_refcounts(&leaf);

    {
        let branch = Rc::new(Node {
            value:    0,
            parent:   RefCell::new(Weak::new()),
            children: RefCell::new(vec![Rc::clone(&leaf)])
        });

        // upgrade() is Option<Rc<_>>
        println!("Parent before = {:?}", leaf.parent.borrow().upgrade());
        show_refcounts(&branch);

        *leaf.parent.borrow_mut() = Rc::downgrade(&branch);

        println!("Parent after = {:?}", leaf.parent.borrow().upgrade());
        show_refcounts(&leaf);
        show_refcounts(&branch);
    }

    println!("Parent after scope = {:?}", leaf.parent.borrow().upgrade());
    show_refcounts(&leaf);
}


fn test_concurrency() {

    // Threads and MPSC channels, go style

    use std::thread;
    use std::time::Duration;

    let vec = vec![1, 2, 3];

    let handle = thread::spawn(move ||
        println!("Here's a vec: {:?}", vec)
    );

    // No way, moved out into closure
    //drop(vec);

    handle.join().unwrap();

    use std::sync::mpsc;

    let (tx, rx) = mpsc::channel();
    let tx1      = tx.clone();

    thread::spawn(move || {
        let vals = vec![
            String::from("Yo, sup"),
            String::from("from"),
            String::from("another thread"),
        ];

        for val in vals {
            tx.send(val).unwrap();
            thread::sleep(Duration::from_millis(1));
        }

        // Nope, vec was moved out during for loop as for .. in calls into_iter()
        //println!("{:?}", vals);
    });

    thread::spawn(move || {
        let vals = vec![
            String::from("This might"),
            String::from("arrive"),
            String::from("mixed up"),
        ];

        for val in vals {
            tx1.send(val).unwrap();
            thread::sleep(Duration::from_millis(1));
        }
    });

    for recv in rx {
        println!("Got '{}'", recv);
    }

    // Shared state concurrency

    use std::sync::{Arc, Mutex};

    // Rc is not Send and so not safe to move a clone across threads
    // Here we could use std::sync::atomic::AtomicUsize
    let countdown   = Arc::new(Mutex::new(10));
    let mut handles = vec![];

    for _ in 0..10 {
        let countdown = Arc::clone(&countdown);
        let handle    = thread::spawn(move || {
            let mut num = countdown.lock().unwrap();
            *num -= 1;
        });

        handles.push(handle);
    }

    for h in handles {
        h.join().unwrap();
    }

    println!("...{}! Liftoff", *countdown.lock().unwrap());

    {
        // Deadlock example

        let m0 = Arc::new(Mutex::new(0));
        let m1 = Arc::new(Mutex::new(1));

        let _work = |flip| {
            let m0 = Arc::clone(&m0);
            let m1 = Arc::clone(&m1);

            thread::spawn(move || {
                if flip {
                    let c0 = m0.lock().unwrap();
                    let c1 = m1.lock().unwrap();
                    println!("Got {c0} and {c1}");
                }

                else {
                    let c0 = m1.lock().unwrap();
                    let c1 = m0.lock().unwrap();
                    println!("Got {c0} and {c1}");
                }
            })
        };

        /*
        let mut handlers = vec![];

        for _ in 0..50 {
            handlers.push(_work(true));
            handlers.push(_work(false));
        }

        for h in handlers {
            h.join().unwrap();
        }
        */
    }
}


fn test_oop() {

    struct AvgCollection {
        list: Vec<isize>,
        avg:  f64
    }


    #[allow(dead_code)]
    impl AvgCollection {

        pub fn add(&mut self, val: isize) {
            self.list.push(val);
            self.update_avg();
        }

        pub fn pop(&mut self) -> Option<isize> {
            let val = self.list.pop();

            if let Some(_) = val {
                self.update_avg();
            }

            val
        }

        pub fn avg(&self) -> f64 {
            self.avg
        }

        fn update_avg(&mut self) {
            self.avg = {
                if self.list.is_empty() {
                    0.0
                } else {
                    self.list.iter().sum::<isize>() as f64 / self.list.len() as f64
                }
            };
        }
    }

    let mut coll = AvgCollection { list: vec![], avg: 0.0 };
    assert_eq!(coll.avg(), 0.0);

    for i in 1..100 {
        coll.add(i);
    }

    assert_eq!(coll.avg(), 50.0);
}


fn test_trait_objects() {

    pub trait Draw {
        fn draw(&self);
    }

    pub struct Screen {
        pub components: Vec<Box<dyn Draw>>
    }

    impl Screen {
        pub fn run(&self) {
            for c in self.components.iter() {
                c.draw();
            }
        }
    }

    pub struct Button {
        pub w:    usize,
        pub h:    usize,
        pub text: String
    }

    impl Draw for Button {
        fn draw(&self) {
            println!("Rendering button: {}", self.text);
        }
    }

    pub struct Label {
        pub text: String
    }

    impl Draw for Label {
        fn draw(&self) {
            println!("Rendering label: {}", self.text);
        }
    }

    let screen = Screen {
        components: vec![
            Box::new(Label { text: String::from("Yo") }),
            Box::new(Button {
                w: 1, h: 2, text: String::from("Clicky clickety clack")
            })
        ]
    };

    screen.run();
}


fn test_oo_pattern() {

    trait State {
        fn request_review(self: Box<Self>) -> Box<dyn State>;
        fn approve(self: Box<Self>) -> Box<dyn State>;
        fn reject(self: Box<Self>) -> Box<dyn State>;
        fn content<'a>(&self, post: &'a Post) -> &'a str;
    }

    enum PostStates {
        Draft,
        PendingReview,
        Published
    }

    use PostStates::*;

    pub struct Post {
        state: Option<Box<dyn State>>,
        content: String
    }

    impl Post {
        pub fn new() -> Post {
            Post {
                state: Some(Box::new(Draft)),
                content: String::new()
            }
        }

        pub fn add_text(&mut self, text: &str) {
            self.content.push_str(text);
        }

        pub fn content(&self) -> &str {
            self.state.as_ref().unwrap().content(self)
        }

        pub fn submit(&mut self) {
            if let Some(s) = self.state.take() {
                self.state = Some(s.request_review());
            }
        }

        pub fn approve(&mut self) {
            if let Some(s) = self.state.take() {
                self.state = Some(s.approve());
            }
        }

        pub fn reject(&mut self) {
            if let Some(s) = self.state.take() {
                self.state = Some(s.reject());
            }
        }
    }

    impl State for PostStates {
        fn request_review(self: Box<Self>) -> Box<dyn State> {
            match *self {
                Draft => Box::new(PendingReview),
                _     => self
            }
        }

        fn approve(self: Box<Self>) -> Box<dyn State> {
            match *self {
                PendingReview => Box::new(Published),
                _             => self
            }
        }

        fn reject(self: Box<Self>) -> Box<dyn State> {
            match *self {
                PendingReview => Box::new(Draft),
                _             => self
            }
        }

        fn content<'a>(&self, post: &'a Post) -> &'a str {
            match *self {
                Draft         => "<Work in progress!>",
                PendingReview => "<Under review!>",
                _             => &post.content
            }
        }
    }

    let mut p = Post::new();
    p.add_text("Test post");
    assert_eq!("<Work in progress!>", p.content());
    p.submit();
    assert_eq!("<Under review!>", p.content());
    p.reject();
    assert_eq!("<Work in progress!>", p.content());
    p.submit();
    assert_eq!("<Under review!>", p.content());
    p.approve();
    assert_eq!("Test post", p.content());
}


fn test_raw_ptrs() {
    let mut x: usize = 42;

    // All cool
    let ptr1 = &x as *const usize;
    let ptr2 = &mut x as *mut usize;
    // We dont actually hold the rust &references as they get casted to raw ptr immediatly
    // Otherwise we'd be holding immutable + mutable ref at the same time, no way

    // But dereferencing is unsafe
    unsafe {
        println!("Pointing to {} (that's {})", *ptr1, *ptr2);
        *ptr2 = 37;
    }

    println!("It's now {}!", x); // spooky action at a distance

    use std::slice;

    fn split_at_mut<T>(values: &mut [T], at: usize) -> (&mut [T], &mut [T]) {
        let len = values.len();
        let ptr = values.as_mut_ptr();
        assert!(at <= len);

        // Doesn't work, attempts double borrow
        // (&mut values[..at], &mut values[at..])

        unsafe {
            ( slice::from_raw_parts_mut(ptr, at)
            , slice::from_raw_parts_mut(ptr.add(at), len - at))
        }
    }

    let mut things = vec!["here", "are", "some", "words"];
    let (begin, end) = split_at_mut(&mut things, 2);

    println!("Split between '{}' and '{}'", begin.last().unwrap(), end[0]);

    {
        let address = 0x01337usize;
        let r = address as *mut i32; // Raw pointer to arbitrary memory

        // Building a slice of length 10000 from that ptr (unsafe)
        let values: &[i32] = unsafe { slice::from_raw_parts_mut(r, 10000) };

        // Simple choice really
        let i_can_ub_in_rust = false;
        if i_can_ub_in_rust { println!("{:?}", values[0]); }
    }
}
